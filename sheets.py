import os
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from pathlib import Path
import typing as t
import subprocess

scope = ["https://spreadsheets.google.com/feeds",
         "https://www.googleapis.com/auth/spreadsheets",
         "https://www.googleapis.com/auth/drive.file",
         "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("creds.json", scope)
client = gspread.authorize(creds)

current_dir = os.path.join(os.getcwd(), 'Tasks')

sheet = client.open("Airlab_test").sheet1


# get nickname of GitLab
def get_nick():
    # get remote info
    remote = subprocess.check_output('git remote -v',
                                     stderr=subprocess.STDOUT,
                                     shell=True
                                     ).decode('cp866')
    # parse remote info
    nick = remote.split('\n')[0].split('/')
    for x in range(len(nick)):
        if nick[x] == 'gitlab.com' or nick[x] == 'git@gitlab.com':
            return nick[x + 1]


def iterdir(directory) -> t.List[str]:
    dirpath = Path(directory)
    file_list = []
    for x in dirpath.iterdir():
        if x.is_dir():
            split_path = str(x).split('/')  # split in docker /, in python \\
            if split_path[len(split_path) - 1].startswith('.'):
                continue
            else:
                file_list.append(split_path[len(split_path) - 1])
    return list(file_list)


def sheet_init():
    counter = 3
    if sheet.row_values(1):
        return 0
    sheet.update_cell(1, 1, 'Дата начала курса')
    sheet.update_cell(2, 1, get_nick())
    sheet.update_cell(2, 2, 0)
    # TODO add datetime of course start
    for module in iterdir(current_dir):
        if module == "__pycache__":
            continue
        sheet.update_cell(counter, 1, module)
        counter += 1


sheet_init()
