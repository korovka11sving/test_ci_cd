import os
import typing as t
from pathlib import Path
from flask import (
    Flask,
    render_template,
)

app = Flask(__name__)


def get_tasks():
    direct = os.getcwd().split('\\')
    path = ""
    for b in direct[:]:
        path += b + "\\"
    return os.path.join(path, 'Tasks')


def iterdir(directory) -> t.List[str]:
    dirpath = Path(directory)
    file_list = []
    for x in dirpath.iterdir():
        print(x)
        if x.is_dir():
            split_path = str(x).split("\\")  # split in docker /, in python \\
            if split_path[len(split_path) - 1].startswith('.') or split_path[len(split_path) - 1].startswith('__'):
                continue
            else:
                file_list.append(split_path[len(split_path) - 1])
    return list(file_list)


dir_list = iterdir(get_tasks())


@app.route('/courses')
def courses():
    python, c, algorithms = "python", "c++", "algorithms"
    template_context = dict(python=python, c=c, algorithms=algorithms)
    return render_template('index.html', **template_context)


@app.route('/courses/python')
def python_course():
    string = "Hello in python course"
    return render_template('courses_template.html', string=string, dir_list=dir_list)


@app.route('/courses/C++')
def c_course():
    string = "Hello in c++ course"
    return render_template('courses_template.html', string=string)


@app.route('/courses/algorithms')
def algorithms_course():
    string = "Hello in algorithms course"
    return render_template('courses_template.html', string=string)


if __name__ == '__main__':
    app.run()
