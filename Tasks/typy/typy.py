# WARNING: don't add additional imports and functions

import typing as tp


def func1(a):
    return a / 2


def func2(a):
    if a is None:
        return None
    else:
        return bool(a)


def func3(a):
    return a[1]


def func4(a):
    if a:
        return a[0]
    return None


def func5(a, b, c, d):
    return a(b, c, d)
