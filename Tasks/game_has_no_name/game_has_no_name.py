import typing as tp


class Player:
    def ready(self, next_game_method: str, next_my_method: str) -> None:
        """
        :param next_game_method: next game class method to call
        :param next_my_method: next method game will call
        """

    def play(self, game: tp.Any) -> None:
        """
        :param game: some class with method start
        """
