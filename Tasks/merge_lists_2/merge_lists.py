import typing as tp


def merge(lists: tp.Sequence[tp.Sequence[int]]) -> tp.Sequence[int]:
    """
    Merge any sizes of lists in one sorted list
    :param lists: list of sorted lists
    :return: merged sorted list
    """
