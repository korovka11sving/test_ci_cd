import dataclasses
import dis
import io
from typing import Any, Callable

import pytest

from . import byteme


@dataclasses.dataclass
class Case:
    func: Callable[..., Any]
    expected_dis_out: str

    def __str__(self) -> str:
        return self.func.__name__


TEST_CASES = [
    Case(
        func=byteme.f0,
        expected_dis_out='''\
   0 LOAD_CONST               0 (None)
   2 RETURN_VALUE
'''
    ),
    Case(
        func=byteme.f1,
        expected_dis_out='''\
   0 LOAD_CONST               1 (0)
   2 STORE_FAST               0 (a)
   4 LOAD_FAST                0 (a)
   6 RETURN_VALUE
'''
    ),
    Case(
        func=byteme.f2,
        expected_dis_out='''\
   0 LOAD_CONST               1 (0)
   2 STORE_FAST               0 (a)
   4 LOAD_GLOBAL              0 (print)
   6 LOAD_FAST                0 (a)
   8 CALL_FUNCTION            1
  10 POP_TOP
  12 LOAD_CONST               0 (None)
  14 RETURN_VALUE
'''
    ),
    Case(
        func=byteme.f3,
        expected_dis_out='''\
   0 LOAD_CONST               1 (0)
   2 STORE_FAST               0 (a)
   4 LOAD_FAST                0 (a)
   6 LOAD_CONST               2 (1)
   8 INPLACE_ADD
  10 STORE_FAST               0 (a)
  12 LOAD_GLOBAL              0 (print)
  14 LOAD_FAST                0 (a)
  16 CALL_FUNCTION            1
  18 POP_TOP
  20 LOAD_CONST               0 (None)
  22 RETURN_VALUE
'''
    ),
    Case(
        func=byteme.f4,
        expected_dis_out='''\
   0 LOAD_GLOBAL              0 (range)
   2 LOAD_CONST               1 (10)
   4 CALL_FUNCTION            1
   6 RETURN_VALUE
'''
    ),
    Case(
        func=byteme.f5,
        expected_dis_out='''\
   0 SETUP_LOOP              24 (to 26)
   2 LOAD_GLOBAL              0 (range)
   4 LOAD_CONST               1 (10)
   6 CALL_FUNCTION            1
   8 GET_ITER
  10 FOR_ITER                12 (to 24)
  12 STORE_FAST               0 (i)
  14 LOAD_GLOBAL              1 (print)
  16 LOAD_FAST                0 (i)
  18 CALL_FUNCTION            1
  20 POP_TOP
  22 JUMP_ABSOLUTE           10
  24 POP_BLOCK
  26 LOAD_CONST               0 (None)
  28 RETURN_VALUE
'''
    ),
    Case(
        func=byteme.f6,
        expected_dis_out='''\
   0 LOAD_CONST               1 (0)
   2 STORE_FAST               0 (a)
   4 SETUP_LOOP              24 (to 30)
   6 LOAD_GLOBAL              0 (range)
   8 LOAD_CONST               2 (10)
  10 CALL_FUNCTION            1
  12 GET_ITER
  14 FOR_ITER                12 (to 28)
  16 STORE_FAST               1 (i)
  18 LOAD_FAST                0 (a)
  20 LOAD_CONST               3 (1)
  22 INPLACE_ADD
  24 STORE_FAST               0 (a)
  26 JUMP_ABSOLUTE           14
  28 POP_BLOCK
  30 LOAD_GLOBAL              1 (print)
  32 LOAD_FAST                0 (a)
  34 CALL_FUNCTION            1
  36 POP_TOP
  38 LOAD_CONST               0 (None)
  40 RETURN_VALUE
'''
    ),
    Case(
        func=byteme.f7,
        expected_dis_out='''\
   0 LOAD_CONST               4 ((1,))
   2 LOAD_CONST               2 ((2, 3))
   4 LOAD_CONST               5 ((4,))
   6 BUILD_LIST_UNPACK        3
   8 RETURN_VALUE
'''
    ),
    Case(
        func=byteme.f8,
        expected_dis_out='''\
   0 LOAD_CONST               1 ((1, 2))
   2 UNPACK_SEQUENCE          2
   4 STORE_FAST               0 (x)
   6 STORE_FAST               1 (y)
   8 LOAD_CONST               0 (None)
  10 RETURN_VALUE
'''
    ),
    Case(
        func=byteme.f9,
        expected_dis_out='''\
   0 LOAD_CONST               1 (1)
   2 LOAD_CONST               1 (1)
   4 COMPARE_OP               2 (==)
   6 POP_JUMP_IF_FALSE       12
   8 LOAD_CONST               1 (1)
  10 RETURN_VALUE
  12 LOAD_CONST               2 (2)
  14 RETURN_VALUE
'''
    ),
    Case(
        func=byteme.f10,
        expected_dis_out='''\
   0 SETUP_LOOP              26 (to 28)
   2 LOAD_GLOBAL              0 (range)
   4 LOAD_CONST               1 (10)
   6 CALL_FUNCTION            1
   8 GET_ITER
  10 FOR_ITER                14 (to 26)
  12 STORE_FAST               0 (i)
  14 LOAD_FAST                0 (i)
  16 LOAD_CONST               2 (3)
  18 COMPARE_OP               2 (==)
  20 POP_JUMP_IF_FALSE       10
  22 BREAK_LOOP
  24 JUMP_ABSOLUTE           10
  26 POP_BLOCK
  28 LOAD_CONST               0 (None)
  30 RETURN_VALUE
'''
    ),
    Case(
        func=byteme.f11,
        expected_dis_out='''\
   0 LOAD_CONST               1 (1)
   2 LOAD_CONST               2 (2)
   4 LOAD_CONST               3 (3)
   6 BUILD_LIST               3
   8 STORE_FAST               0 (list_)
  10 LOAD_CONST               1 (1)
  12 LOAD_CONST               2 (2)
  14 LOAD_CONST               4 (('a', 'b'))
  16 BUILD_CONST_KEY_MAP      2
  18 STORE_FAST               1 (dict_)
  20 LOAD_FAST                0 (list_)
  22 LOAD_FAST                1 (dict_)
  24 BUILD_TUPLE              2
  26 RETURN_VALUE
'''
    ),
    Case(
        func=byteme.f12,
        expected_dis_out='''\
   0 LOAD_CONST               1 (1)
   2 STORE_FAST               0 (a)
   4 LOAD_CONST               2 (2)
   6 STORE_FAST               1 (b)
   8 LOAD_CONST               3 (3)
  10 STORE_FAST               2 (c)
  12 LOAD_CONST               4 (4)
  14 STORE_FAST               3 (d)
  16 LOAD_CONST               5 (5)
  18 STORE_FAST               4 (e)
  20 LOAD_FAST                0 (a)
  22 LOAD_FAST                1 (b)
  24 LOAD_FAST                2 (c)
  26 BINARY_MULTIPLY
  28 LOAD_FAST                3 (d)
  30 LOAD_FAST                4 (e)
  32 BINARY_POWER
  34 BINARY_TRUE_DIVIDE
  36 BINARY_ADD
  38 RETURN_VALUE
'''
    ),
]


def strip_dis_out(dis_out: str) -> str:
    """Strip first 11 chars from dis_out and remove empty lines"""
    return '\n'.join(line[11:] for line in dis_out.split('\n') if line) + '\n'


@pytest.mark.parametrize('t', TEST_CASES, ids=str)
def test_byteme(t: Case) -> None:
    out = io.StringIO()
    dis.dis(t.func, file=out)
    actual_dis_out = out.getvalue()
    assert strip_dis_out(actual_dis_out) == t.expected_dis_out
