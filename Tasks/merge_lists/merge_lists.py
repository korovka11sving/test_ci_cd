import typing as tp


def merge(a: tp.Sequence[int], b: tp.Sequence[int]) -> tp.Sequence[int]:
    """
    Merge two sorted lists in one sorted list
    :param a: first sorted list
    :param b: second sorted list
    :return: merged sorted list
    """
