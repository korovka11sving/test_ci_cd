import types
import typing as tp


def count_operations(source_code: types.CodeType) -> tp.Dict[str, int]:
    """Count byte code operations in given source code.

    :param source_code: the bytecode operation names to be extracted from
    :return: operation counts
    """
