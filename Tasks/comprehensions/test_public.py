import copy
import itertools

import typing as tp


from . import comprehensions as comp


def get_range_with_peak_on_position(range_size: int, position: int) -> tp.List[int]:
    if position >= range_size or position < 0:
        raise ValueError("Position should be in [0, range_size)")

    return list(itertools.chain(range(position), [range_size + 1], range(range_size - position - 1, position, -1)))


TEST_RECORDS: tp.List[tp.Mapping[str, tp.Any]] = [
    {"EventID": 12345, "EventTime": 1568839214, "UserID": 12456,
     "PageID": 10, "RegionID": None, "DeviceType": "Safari"},
    {"EventID": 12346, "EventTime": 1568839215, "UserID": 12456, "PageID": 10, "RegionID": None,
     "DeviceType": "Safari"},
    {"EventID": 12347, "EventTime": 1568839216, "UserID": 12456, "PageID": 11, "RegionID": None,
     "DeviceType": "Safari"},
    {"EventID": 25647, "EventTime": 1568839217, "UserID": 12395, "PageID": 112, "RegionID": 10,
     "DeviceType": "Internet Explorer"},
    {"EventID": 12345, "EventTime": 1568839218, "UserID": 12395, "PageID": 221, "RegionID": 0,
     "DeviceType": "Firefox"},
    {"EventID": 15789, "EventTime": 1568839219, "UserID": 12399, "PageID": 221, "RegionID": 20,
     "DeviceType": "Internet Explorer"}
]


TEST_RECORD: tp.Mapping[str, tp.Any] = TEST_RECORDS[0]


def test_get_unique_page_ids() -> None:
    test_records = copy.deepcopy(TEST_RECORDS)
    result = comp.get_unique_page_ids(test_records)

    assert test_records == TEST_RECORDS, "You shouldn't change inputs"
    assert result == {10, 11, 112, 221}


def test_get_unique_page_ids_visited_after_ts() -> None:
    test_records = copy.deepcopy(TEST_RECORDS)
    result = comp.get_unique_page_ids_visited_after_ts(test_records, 1568839216)

    assert test_records == TEST_RECORDS, "You shouldn't change inputs"
    assert result == {112, 221}


def test_get_unique_user_ids_visited_page_after_ts() -> None:
    test_records = copy.deepcopy(TEST_RECORDS)
    result = comp.get_unique_user_ids_visited_page_after_ts(test_records, 1568839216, 221)

    assert test_records == TEST_RECORDS, "You shouldn't change inputs"
    assert result == {12395, 12399}


def test_get_events_by_device_type() -> None:
    test_records = copy.deepcopy(TEST_RECORDS)
    result = comp.get_events_by_device_type(test_records, "Internet Explorer")

    assert test_records == TEST_RECORDS, "You shouldn't change inputs"
    assert result == [
        {"EventID": 25647, "EventTime": 1568839217, "UserID": 12395, "PageID": 112, "RegionID": 10,
         "DeviceType": "Internet Explorer"},
        {"EventID": 15789, "EventTime": 1568839219, "UserID": 12399, "PageID": 221, "RegionID": 20,
         "DeviceType": "Internet Explorer"}
    ]


def test_get_region_ids_with_none_replaces_by_default() -> None:
    test_records = copy.deepcopy(TEST_RECORDS)
    result = comp.get_region_ids_with_none_replaces_by_default(test_records)

    assert test_records == TEST_RECORDS, "You shouldn't change inputs"
    assert result == [100500, 100500, 100500, 10, 0, 20]


def test_get_region_id_if_not_none() -> None:
    test_records = copy.deepcopy(TEST_RECORDS)
    result = comp.get_region_id_if_not_none(test_records)

    assert test_records == TEST_RECORDS, "You shouldn't change inputs"
    assert result == [10, 0, 20]


def test_get_keys_where_value_is_not_none() -> None:
    test_r = copy.deepcopy(TEST_RECORD)
    result = comp.get_keys_where_value_is_not_none(test_r)

    assert test_r == TEST_RECORD, "You shouldn't change inputs"
    assert sorted(result) == sorted(["EventID", "EventTime", "UserID", "PageID", "DeviceType"])


def test_get_record_with_none_if_key_not_in_keys() -> None:
    test_r = copy.deepcopy(TEST_RECORD)
    result = comp.get_record_with_none_if_key_not_in_keys(test_r, {"EventID", "UserID"})

    assert test_r == TEST_RECORD, "You shouldn't change inputs"
    assert result == {"EventID": 12345, "EventTime": None, "UserID": 12456, "PageID": None, "RegionID": None,
                      "DeviceType": None}


def test_get_record_with_key_in_keys() -> None:
    test_r = copy.deepcopy(TEST_RECORD)
    result = comp.get_record_with_key_in_keys(test_r, {"EventID", "UserID"})

    assert test_r == TEST_RECORD, "You shouldn't change inputs"
    assert result == {"EventID": 12345, "UserID": 12456}


def test_get_keys_if_key_in_keys() -> None:
    test_r = copy.deepcopy(TEST_RECORD)
    result = comp.get_keys_if_key_in_keys(test_r, {"EventID", "UserID", "SomeField"})

    assert test_r == TEST_RECORD, "You shouldn't change inputs"
    assert result == {"EventID", "UserID"}
