import copy

import dataclasses
import itertools
import random
import string

import typing as tp

import pytest


from .alphabet import get_alphabet, build_graph


@dataclasses.dataclass
class Case:
    words: tp.List[str]
    result: tp.Dict[str, tp.List[str]]

    def __str__(self) -> str:
        return str(self.words)


TEST_CASES = [
    Case(words=[],
         result={}),
    Case(words=["aa", "aab"],
         result={"a": [], "b": []}),
    Case(words=["aabcde"],
         result={"a": [], "b": [], "c": [], "d": [], "e": []}),
    Case(words=["aa", "aab", "acb"],
         result={"a": ["c"], "b": [], "c": []}),
    Case(words=["aaa", "bbb", "cccc"],
         result={"a": ["b"], "b": ["c"], "c": []}),
    Case(words=["aa", "aa", "aa"],
         result={"a": []}),
    Case(words=["aab", "aac", "aad"],
         result={"b": ["c"], "c": ["d"], "a": [], "d": []}),
    Case(words=["aa", "aac", "aab"],
         result={"c": ["b"], "a": [], "b": []}),
    Case(words=["aab", "aac", "dee", "deeer"],
         result={"b": ["c"], "a": ["d"], "c": [], "d": [], "e": [], "r": []}),
    Case(words=["aawq", "aad", "aade"],
         result={"w": ["d"], "q": [], "a": [], "d": [], "e": []}),
    Case(words=["aawq", "aad", "aadf", "fdaa", "ffdd"],
         result={"w": ["d"], "a": ["f"], "d": ["f"], "f": [], "q": []}),
    Case(words=["aawq", "bced", "bcddd"],
         result={"a": ["b"], "e": ["d"], "b": [], "c": [], "d": [], "q": [], "w": []}),
    Case(words=["aa", "aab", "aacb", "aacf", "aacc", "dacdf", "dfghr", "dfchr", "zfdcf", "zxdcf", "zxdcr"],
         result={"b": ["c", "f"], "a": ["d", "f"], "d": ["z"], "z": [], "x": [],
                 "f": ["c", "x", "r"], "g": ["c"], "c": [], "h": [], "r": []}),
    Case(words=["agc", "bha", "ebc", "ebe", "eeb"],
         result={"a": ["b"], "b": ["e"], "c": ["e"], "h": [], "g": [], "e": []}),
    Case(words=["a", "b", "ba", "bc", "bca", "bcd", "bcda", "bcde"],
         result={"a": ["b", "c", "d", "e"], "b": [], "c": [], "d": [], "e": []})
]


def _generate_words(n: int) -> tp.Set[str]:

    words: tp.Set[str] = set()

    while len(words) != n:
        word_len = random.choice(range(3, 6))
        words.add("".join(random.choices(string.ascii_letters, k=word_len)))

    return words


def test_random_stress() -> None:
    alphabet = "".join(random.sample(string.ascii_letters, len(string.ascii_letters)))
    words = sorted(_generate_words(100000), key=lambda word: tuple([alphabet.index(c) for c in word]))
    graph = build_graph(words)

    words_copy = copy.deepcopy(words)

    result_alphabet = get_alphabet(words_copy)

    assert words_copy == words, "You shouldn't change inputs"

    assert sorted(result_alphabet) == sorted(alphabet)

    for i, letter in enumerate(result_alphabet):
        if letter in graph:
            for c in graph[letter]:
                assert result_alphabet.index(c) > i


@pytest.mark.parametrize("t", TEST_CASES, ids=str)
def test_build_graph(t: Case) -> None:
    words_copy = copy.deepcopy(t.words)

    graph = build_graph(words_copy)
    assert t.words == words_copy, "You shouldn't change inputs"

    graph_ordered = {key: sorted(values) for key, values in graph.items()}
    expected_graph_ordered = {key: sorted(values) for key, values in t.result.items()}

    assert graph_ordered == expected_graph_ordered


@pytest.mark.parametrize("t", TEST_CASES, ids=str)
def test_get_alphabet(t: Case) -> None:
    words_copy = copy.deepcopy(t.words)

    graph = build_graph(words_copy)
    answer = get_alphabet(words_copy)

    assert t.words == words_copy, "You shouldn't change inputs"

    all_letters = sorted(set(itertools.chain(*t.words)))

    assert sorted(answer) == all_letters

    for i, letter in enumerate(answer):
        if letter in graph:
            for c in graph[letter]:
                assert answer.index(c) > i
