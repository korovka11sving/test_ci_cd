import math
from typing import Any, Dict, Optional

PROMPT = '>>> '


def run_calc(context: Optional[Dict[str, Any]] = None) -> None:
    """Run interactive calculator session in specified namespace"""


if __name__ == '__main__':
    context = {'math': math}
    run_calc(context)
