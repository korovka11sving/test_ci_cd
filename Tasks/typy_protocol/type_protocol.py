import typing as tp
from typing_extensions import Protocol


class Gettable:
    pass


def get(container, index):
    if container:
        return container[index]

    return None
