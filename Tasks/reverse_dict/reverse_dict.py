import typing as tp

from collections import defaultdict


def revert(dct: tp.Mapping[str, str]) -> tp.Mapping[str, tp.List[str]]:
    """
    :param dct: dictionary to revert in format {key: value}
    :return: reverted dictionary {value: [key1, key2, key3]}
    """
