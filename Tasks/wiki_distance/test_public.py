from dataclasses import dataclass
from urllib.parse import quote
from typing import Optional

import pytest

from .wiki_distance import distance


def wiki_url(article: str) -> str:
    return 'https://ru.wikipedia.org/wiki/' + quote(article)


@dataclass
class Case:
    source_article: str
    target_article: str
    distance: Optional[int]

    def __str__(self) -> str:
        return f'{self.source_article} -> {self.target_article}'


CASES = [
    Case(
        source_article='Git',
        target_article='Система_управления_версиями',
        distance=1
    ),
    Case(
        source_article='Предмет',
        target_article='Философия',
        distance=None
    ),
    Case(
        source_article='Гравитация',
        target_article='Римляне',
        distance=2
    ),
    Case(
        source_article='Математика',
        target_article='Философия',
        distance=43
    ),
    Case(
        source_article='Linux',
        target_article='Философия',
        distance=47
    ),
    Case(
        source_article='Python',
        target_article='Философия',
        distance=46
    )
]


@pytest.mark.parametrize('case', CASES, ids=str)
def test_distance(case: Case) -> None:
    assert distance(wiki_url(case.source_article), wiki_url(case.target_article)) == case.distance
