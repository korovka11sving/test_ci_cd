import typing as tp


def convert_to_common_type(data: tp.List[tp.Any]) -> tp.List[tp.Any]:
    """
    Takes list of multiple types' elements and convert each element to common type according to given rules
    :param data: list of multiple types' elements
    :return: list with elements converted to common type
    """
