import typing as tp


def reverse(a: tp.Sequence[int]) -> tp.Sequence[int]:
    """
    Return reversed list
    :param a: input list
    :return: reversed list
    """


def reverse_inplace(a: tp.MutableSequence[int]) -> None:
    """
    Revert list inplace
    :param a: input list
    :return: None
    """
