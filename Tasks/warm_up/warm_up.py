from typing import List, Generator, Dict, Set, Any


def transpose(matrix: List[List[Any]]) -> List[List[Any]]:
    """
    :param matrix: rectangular matrix
    :return: transposed matrix
    """


def uniq(sequence: List[Any]) -> Generator[Any, None, None]:
    """
    :param sequence: arbitrary sequence of comparable elements
    :return: generator of elements of `sequence` in
    the same order without duplicates
    """


def dict_merge(*dicts: Dict[Any, Any]) -> Dict[Any, Any]:
    """
    :param *dicts: flat dictionaries to be merged
    :return: merged dictionary
    """


def product(lhs: List[int], rhs: List[int]) -> int:
    """
    :param rhs: first factor
    :param lhs: second factor
    :return: scalar product
    """
