from concurrent.futures import ThreadPoolExecutor, as_completed
from typing import Sequence, Tuple, Optional, Dict

import requests


CHAIN_MAX_LEN = 8


def all_languages() -> Sequence[str]:
    """
    :return: sequence of languages supported by the API,
    e.g. "en", "de" and so on
    """


def translate(text: str, lang: str) -> str:
    """
    :param text: any string with text for translation
    :param lang: target language for translation
    :return: translated text
    """


def translation_chain_length(text: str, lang: str) -> Tuple[str, Optional[int]]:
    """Counts how many times given text needed to be translate back and forth to stop changing.
    Assuming that single iteration is a thanslation in both directions.
    IMPORTANT: The comparison should be made with the text as it was **before** the iteration,
    not with the original one for the chain.
    :param text: any string with text
    :param lang: target language
    :return: length of the translation chain for text after which it stop changing or None
    if the length becomes more than CHAIN_MAX_LEN.
    """


def compute_chains(text: str, langs: Sequence[str]) -> Dict[str, Optional[int]]:
    """Computes chains lengths for requested languages in parallel
    :param text: string with text to compute chains lengths
    :param langs: sequence of languages
    :return: mapping with lengths of translation chains for every requested language
    """
    result = {}
    with ThreadPoolExecutor() as executor:  # multithreading executor to perform translations in parallel
        futures = []
        for lang in langs:
            futures.append(
                executor.submit(translation_chain_length, text, lang)
            )

        for future in as_completed(futures):
            lang, length = future.result()
            result[lang] = length
            # print(lang, length)  # uncomment to see results as soon as they appear

    return result
