from dataclasses import dataclass
from unittest.mock import Mock, patch
from typing import List, Dict, Any

import pytest

from .translation_chains import all_languages, translate, compute_chains


@dataclass
class GetLanguagesCase:
    api_result: Dict[str, Any]
    languages: List[str]

    def __str__(self) -> str:
        return str(self.languages)


LANGUAGE_CASES = [
    GetLanguagesCase(
        api_result={'langs': {}},
        languages=[]
    ),
    GetLanguagesCase(
        api_result={'langs': {'en': 'Английский', 'ru': 'Русский'}},
        languages=['en', 'ru']
    ),
    GetLanguagesCase(
        api_result={'langs': {'en': 'Английский', 'ru': 'Русский', 'de': 'Немецкий'}},
        languages=['en', 'ru', 'de']
    ),
    GetLanguagesCase(
        api_result={'langs': {'my': 'Бирманский', 'ne': 'Непальский', 'nl': 'Нидерландский', 'no': 'Норвежский'}},
        languages=['my', 'ne', 'nl', 'no']
    )
]


@pytest.mark.parametrize('case', LANGUAGE_CASES, ids=str)
def test_get_languages(case: GetLanguagesCase) -> None:
    json_payload_mock = Mock(return_value=case.api_result)
    response_mock = Mock(return_value=Mock(status_code=200, json=json_payload_mock))

    with patch('requests.api.request', new=response_mock):
        assert set(all_languages()) == set(case.languages)


@dataclass
class TranslationCase:
    text: str
    lang: str
    api_result: Dict[str, Any]
    translation: str

    def __str__(self) -> str:
        return self.translation[10:]


TRANSLATION_CASES = [
    TranslationCase(
        text='У лукоморья дуб зеленый',
        lang='en',
        api_result={'code': 200, 'lang': 'ru-en', 'text': ['There stands a green oak']},
        translation='There stands a green oak'
    ),
    TranslationCase(
        text='The Hitchhiker\'s Guide to the Galaxy',
        lang='ru',
        api_result={'code': 200, 'lang': 'en-ru', 'text': ['Автостопом по Галактике']},
        translation='Автостопом по Галактике'
    ),
    TranslationCase(
        text=(
            'Beautiful is better than ugly.\nExplicit is better than implicit.\n'
            'Simple is better than complex.\nComplex is better than complicated.'
        ),
        lang='ru',
        api_result={'code': 200, 'lang': 'ru-en', 'text': [
            'Красиво лучше, чем безобразно.\n'
            'Явное лучше, чем неявное.\n'
            'Простое лучше, чем сложное.\n'
            'Сложный лучше, чем сложный.'
        ]},
        translation=(
            'Красиво лучше, чем безобразно.\n'
            'Явное лучше, чем неявное.\n'
            'Простое лучше, чем сложное.\n'
            'Сложный лучше, чем сложный.'
        )
    ),
]


@pytest.mark.parametrize('case', TRANSLATION_CASES, ids=str)
def test_translations(case: TranslationCase) -> None:
    json_payload_mock = Mock(return_value=case.api_result)
    response_mock = Mock(return_value=Mock(status_code=200, json=json_payload_mock))

    with patch('requests.api.request', new=response_mock):
        assert translate(case.text, case.lang) == case.translation


def test_translation_chains_simple() -> None:
    result = compute_chains('Добро пожаловать', ['en', 'de', 'fr', 'be', 'es', 'it'])

    assert result == {'en': 1, 'de': 1, 'fr': 1, 'be': 1, 'es': 1, 'it': 1}


def test_translation_chains_longer_text() -> None:
    result = compute_chains('''\
Это самое дурацкое в магии. Ты двадцать лет тратишь на то, чтобы выучить заклинание и вызвать к себе в спальню
обнаженных девственниц, но к тому времени ты насквозь пропитываешься ртутными парами, а твои глаза перестают видеть,
испорченные чтением старых гримуаров. Ты даже вспомнить не сможешь, зачем тебе эти девственницы понадобились.
''', ['en', 'de'])

    assert result == {'en': 3, 'de': 5}


def test_translation_chains_complex() -> None:
    languages = [lang for lang in all_languages() if 'e' in lang]
    assert set(languages) == {'be', 'ceb', 'de', 'el', 'en', 'eo', 'es', 'et', 'eu', 'he', 'ne', 'te'}

    result = compute_chains('''\
В начале семидесятых он услышал по радио Би-би-си о пользе разговоров с растениями и решил, что это отличная идея. Хотя
слово «разговор», возможно, не совсем верно характеризует его действия. Своими речами он держал их в страхе Божьем. А
точнее, в страхе перед Кроули. В дополнение к разговорам каждую пару месяцев Кроули выбирал цветок, который рос слишком
медленно или его листья начинали увядать и буреть, – в общем, тот, что выглядел чуть хуже других, – и показывал его
остальным растениям. «Попрощайтесь с вашим приятелем, – говорил он им. – Эх, молодо-зелено…» Затем он покидал квартиру
с провинившимся растением в руках и возвращался через час или около того с пустым цветочным горшком, который специально
ставил где-нибудь на видном месте. Его растения были самыми роскошными, зелеными и красивыми во всем Лондоне. И самыми
запуганными.
''', languages)

    assert result == {'be': 2, 'ceb': 7, 'de': None, 'el': 4, 'en': 4, 'eo': 4,
                      'es': 3, 'et': 3, 'eu': 6, 'he': None, 'ne': 7, 'te': 7}
