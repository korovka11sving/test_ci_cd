import copy
import dataclasses
import typing as tp

import pytest

from .banner_search_system import build_index, get_banners, normalize, get_words


@dataclasses.dataclass
class IndexCase:
    banners: tp.List[str]
    result: tp.Dict[str, tp.List[int]]


INDEX_TEST_CASE = [
    IndexCase(banners=["Стиральный машина - Более 1000 модель!"], result={
        "модель": [0],
        "более": [0],
        "стиральный": [0],
        "машина": [0],
    }),
    IndexCase(banners=["Стильный джинсы со скидка - Скидка 15% от 2999 р"], result={
        'стильный': [0],
        "джинсы": [0],
        "скидка": [0]
    }),
    IndexCase(banners=[
        "Стильный джинсы со скидка - Скидка 15% от 2999 р",
        "Ремонт стиральный машина - Скидка -20%",
    ], result={
        'стильный': [0],
        "джинсы": [0],
        "скидка": [0, 1],
        "ремонт": [1],
        "стиральный": [1],
        "машина": [1],
    }),
    IndexCase(banners=[], result={}),
    IndexCase(banners=[
        "Джинсы джинсы джинсы",
        "Джинсы джинсы джинсы",
    ], result={
        "джинсы": [0, 1]
    }),
    IndexCase(banners=[
        "Джинсы холодильник джинсы",
        "Джинсы машинка джинсы",
    ], result={
        "джинсы": [0, 1],
        "холодильник": [0],
        "машинка": [1]
    }),
    IndexCase(banners=[
        "Джинсы холодильник джинсы машинка",
        "холодильник Джинсы машинка джинсы",
    ], result={
        "джинсы": [0, 1],
        "холодильник": [0, 1],
        "машинка": [0, 1]
    }),
]


@pytest.mark.parametrize('t', INDEX_TEST_CASE)
def test_build_index(t: IndexCase) -> None:
    banners_copy = copy.deepcopy(t.banners)
    index = build_index(banners_copy)

    assert t.banners == banners_copy, "You shouldn't change inputs"
    assert t.result == index


@dataclasses.dataclass
class NormalizeCase:
    text: str
    normalized_text: str


NORMALIZE_TEST_CASE = [
    NormalizeCase(text="?#@*&%", normalized_text=""),
    NormalizeCase(text="Холодильник бесплатно!", normalized_text="холодильник бесплатно"),
    NormalizeCase(text="скидка - Скидка 15% от 2999 р", normalized_text="скидка  скидка  от  р"),
    NormalizeCase(text="", normalized_text=""),
    NormalizeCase(text="Джинсы со скидка 600р", normalized_text="джинсы со скидка р")
]


@pytest.mark.parametrize('t', NORMALIZE_TEST_CASE)
def test_normalize(t: NormalizeCase) -> None:
    assert normalize(t.text) == t.normalized_text


@dataclasses.dataclass
class WordsCase:
    query: str
    words: tp.List[str]


GET_WORDS_TEST_CASE = [
    WordsCase(query="", words=[]),
    WordsCase(query="  ", words=[]),
    WordsCase(query="скидка ", words=["скидка"]),
    WordsCase(query="скидка по номинал", words=["скидка", "номинал"]),
    WordsCase(query="скидка  скидка  от  руб    ", words=["скидка", "скидка"]),
    WordsCase(query="скидка  джинсы   и холодильник в подарок  ", words=["скидка", "джинсы", "холодильник", "подарок"])
]


@pytest.mark.parametrize('t', GET_WORDS_TEST_CASE)
def test_get_words(t: WordsCase) -> None:
    assert get_words(t.query) == t.words


@dataclasses.dataclass
class QueryCase:
    query: str
    banners_indices: tp.Sequence[int]


QUERIES_TEST_CASE = [
    QueryCase(query="Купить Москва холодильник", banners_indices=[]),
    QueryCase(query="Купить холодильник владикавказ", banners_indices=[]),
    QueryCase(query="Купить холодильник", banners_indices=[12]),
    QueryCase(query="Из москва холодильник", banners_indices=[10, 14, 17]),
    QueryCase(query="Ремонт итд холодильник", banners_indices=[14, 15, 16, 17]),
    QueryCase(query="Джинсы со скидка 600р", banners_indices=[18, 20, 21]),
    QueryCase(query="Ремонт стиральная машина", banners_indices=[5, 6]),
    QueryCase(query="ozon.ru", banners_indices=[12, 19])
]

BANNERS = [
    "Стиральный машина - Более 1000 модель!",
    "Маленькая стиральный машина - Машина недорого!",
    "Стиральная машина более 545 модель / holodilnik.ru",
    "Стиральная машина ведущий бренд - Неделя скидка!",
    "Стиральная машина - Кэшбэк до 20%",
    "Ремонт стиральная машина Выезд - 0 ₽. - Диагностика - 0 ₽.",
    "Ремонт стиральная машина - Скидка -20%",
    "Починка Стиральная Машина - Быстро и Недорого!",
    "Отремонтировать стиральная машина - на дом от 500₽!",
    "Холодильник.Ру - интернет магазин / holodilnik.ru",
    "Холодильник в Москва! - С гарантия 5 лет!",
    "Холодильник на goods.ru - Кэшбэк до 20%",
    "Купить холодильник на OZON.ru - Доставить завтра",
    "Десяток бренд холодильник - Неделя скидка!",
    "Ремонт холодильник! На дом - в Москва, вызов на дом!",
    "Ремонт холодильник? - Приехать прямо сейчас!",
    "Ремонт холодильник - Ремонт холодильник. Гарантия!",
    "Ремонт холодильник - В Москва и МО",
    "Скидка на джинсы - Покупать на Wildberries!",
    "Джинсы Levis / ozon.ru",
    "Стильный джинсы со скидка - Скидка 15% от 2999 р",
    "Джинсы от 600 руб. на Беру - Скидка и акция каждый день"
]


BANNERS_INDEX = build_index(BANNERS)


@pytest.mark.parametrize('t', QUERIES_TEST_CASE)
def test_get_banners(t: QueryCase) -> None:
    assert get_banners(t.query, BANNERS_INDEX, BANNERS) == [BANNERS[i] for i in t.banners_indices]
