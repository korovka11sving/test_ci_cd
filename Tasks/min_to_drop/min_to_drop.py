import typing as tp


def get_min_to_drop(l: tp.Sequence[tp.Any]) -> int:
    """
    :param l: list of elements
    :return: min number of elements need to drop to leave equal elements
    """
