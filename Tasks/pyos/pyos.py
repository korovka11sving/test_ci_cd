from queue import Queue
from abc import ABC, abstractmethod
from typing import Generator, Optional, Any, Dict, List


class SystemCall(ABC):
    """SystemCall yielded by Task to handle with Scheduler"""

    @abstractmethod
    def handle(self, scheduler: 'Scheduler', task: 'Task') -> bool:
        """
        :param scheduler: link to scheduler to manipulate with active tasks
        :param task: task which requested the system call
        :return: an indication that the task must be scheduled again
        """


Corotine = Generator[Optional[SystemCall], Any, None]


class Task:
    def __init__(self, task_id: int, target: Corotine):
        """
        :param task_id: id of the task
        :param target: corotine to run. Corotine can produce system calls.
        System calls are being executed by scheduler and the result sends back to corotine.
        """

    def set_syscall_result(self, result: Any) -> None:
        """
        Saves result of the last system call
        """

    def step(self) -> Optional[SystemCall]:
        """
        Performs one step of corotine, i.e. sends result of last system call
        to corotine (generator), gets yielded value and returns it.
        """


class Scheduler:
    """Scheduler to manipulate with tasks"""

    def __init__(self) -> None:
        self.task_id = 0
        self.task_queue: Queue[Task] = Queue()
        self.task_map: Dict[int, Task] = {}  # task_id -> task
        self.wait_map: Dict[int, List[Task]] = {}  # task_id -> list of waiting tasks

    def new(self, target: Corotine) -> int:
        """Create and schedule new task
        :param target: corotine to wrap in task
        :return: id of newly created task
        """

    def _schedule_task(self, task: Task) -> None:
        """Add task into task queue
        :param task: task to schedule for execution
        """

    def exit_task(self, task_id: int) -> bool:
        """PRIVATE API: can be used only from scheduler itself or system calls
        Hint: do not forget to reschedule waiting tasks
        :param task: task to remove from scheduler
        :return: true if task id is valid
        """

    def wait_task(self, task_id: int, wait_id: int) -> bool:
        """PRIVATE API: can be used only from scheduler itself or system calls
        :param task: task to hold on until another task is finished
        :param wait_id: id of the other task to wait for
        :return: true if task and wait ids are valid task ids
        """

    def run(self, ticks: Optional[int] = None) -> None:
        """Executes tasks consequentely, gets yielded system calls,
        handles them and reschedules task if needed
        :param ticks: number of iterations (task steps), infinite if not passed
        """

    def empty(self) -> bool:
        """Checks if there are some scheduled tasks"""
        return not bool(self.task_map)


class GetTid(SystemCall):
    """System call to get current task id"""

    def handle(self, scheduler: Scheduler, task: Task) -> bool:
        pass


class NewTask(SystemCall):
    """System call to create new task from target corotine"""

    def __init__(self, target: Corotine):
        self.target = target

    def handle(self, scheduler: Scheduler, task: Task) -> bool:
        pass


class KillTask(SystemCall):
    """System call to kill task with particular task id"""

    def __init__(self, task_id: int):
        self.task_id = task_id

    def handle(self, scheduler: Scheduler, task: Task) -> bool:
        pass


class WaitTask(SystemCall):
    """System call to wait task with particular task id"""

    def __init__(self, task_id: int):
        self.task_id = task_id

    def handle(self, scheduler: Scheduler, task: Task) -> bool:
        # Note: One shouldn't reschedule task which is waiting for another one.
        # But one must reschedule task if task id to wait for is invalid.
        pass
