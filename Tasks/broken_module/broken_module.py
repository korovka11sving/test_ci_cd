from typing import Any, Dict


def force_load(module_name: str) -> Dict[str, Any]:
    """Import module by name, removing all lines which cause exceptions"""
