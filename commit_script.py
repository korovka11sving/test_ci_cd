import subprocess
from sheets import sheet


def cmd(command: str = None, command_list: list = None):
    if command_list is not None:
        return subprocess.call(command_list)
    else:
        try:
            return subprocess.check_output(command,
                                           stderr=subprocess.STDOUT,
                                           shell=True
                                           ).decode('cp866')
        except Exception as e:
            print('Произошла ошибка')
            raise e


def update_mark(module_name: str, mark: int) -> None:
    row_pointer = 3
    while True:
        if module_name == sheet.row_values(row_pointer)[0]:
            sheet.update_cell(row_pointer, 2, mark)
            break
        else:
            row_pointer += 1


# get changed files
def module_test() -> None:
    commit = cmd('git log --oneline -1')
    out = cmd(f'git show {commit[:7]}')
    changed_files = []
    for info in out.split('\n'):
        if info.startswith('+++') and info.endswith('py'):
            # было 0 стало 1
            if len(info[6:].split('/')) == 1:
                continue
            else:
                changed_files.append(info[6:].split('/')[1])
    print(f'Измененные файлы: {set(changed_files)}')
    # test changed files with flake8, mypy, pytest
    for module in set(changed_files):
        if module.endswith('py'):
            continue
        else:
            update_mark(module, 0)
            print(cmd(f'python -m mypy Tasks/{module} '
                      f'--ignore-missing-imports'))
            cmd(f'python -m flake8 Tasks/{module}')
            print(cmd(command_list=['python',
                                    '-m',
                                    'pytest',
                                    f'Tasks/{module}/test_public.py']))
            cmd(f'python -m pytest Tasks/{module}/test_public.py')
            update_mark(module, 100)


module_test()
